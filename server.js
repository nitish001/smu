var express = require('express');
var httpProxy = require('http-proxy');
var bodyParser = require('body-parser');

var apiForwardingUrl = 'http://qa.starmeup.com/';
var proxyOptions = {
    changeOrigin: true
};
var server = express();
server.set('port', 9000);
server.use(express.static(__dirname ));

var apiProxy = httpProxy.createProxyServer(proxyOptions);

console.log('Forwarding API requests to ' + apiForwardingUrl);

// Grab all requests to the server with "/space/".

server.all("/starmeup-api/*", function(req, res) {
    //console.log(req.path);

    apiProxy.web(req, res, {target: apiForwardingUrl});
});

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({
    extended: true
}));


server.listen(server.get('port'), function() {
    console.log('Express server listening on port ' + server.get('port'));
});
